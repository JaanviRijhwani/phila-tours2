//This is not treated as code this is treated is module, so it has to be exported using modules.export = module;

class Person{
    constructor(fullName, favColor){
        this.name = fullName;
        this.favColor = favColor;
    }
    greet(){
        console.log("Hello Welcome! My name is " + this.name + " and I love " + this.favColor + " color!");
    }
}

//module.exports = Person;
//This will be automatically exported!
//console.log("Hello from Person");

export default Person;
//export exportingName Person
//import exportingName from "path"
//those who allow to write name are known as exportingName and the always allow to export one only!
//those who don't allow they write default
//default always allow only one to export