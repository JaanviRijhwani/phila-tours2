var gulp = require('gulp'),
    webpack  = require('webpack');

gulp.task('scripts', function(callback){
     //you can use require and catch webpack.config.js into a object also and pass it here instead of this. You cannot write direct path as it dosen't take String path in arguments.
    webpack(require('../../webpack.config.js'), function(err, stats){
        if(err){
            console.log(err.toString());
        }
        console.log(stats.toString());
        callback();//This is another way of async function instead of return, when you call callback() the thread will be killed
   }); 
});